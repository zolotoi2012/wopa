<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity(repositoryClass=TestRepository::class)
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SWG\Property(description="The unique identifier of the user.")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SWG\Property(type="string", maxLength=255)
     */
    private $еуtest;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getеуtest(): ?string
    {
        return $this->еуtest;
    }

    public function setеуtest(?string $еуtest): self
    {
        $this->еуtest = $еуtest;

        return $this;
    }
}
